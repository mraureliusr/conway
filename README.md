# Conway's Game Of Life
### Because of course we need another implementation

I've been told you're not a real programmer unless you've written a Conway Game
of Life program. So I figured I'd better get it out of the way sooner or
later, and so here we are.

Written in C because Python would probably have been too easy. Was considering
doing it in C++ so I could take advantage of classes.

Currently it uses the 2 array algorithm, copying to a temporary array to process
and then copying back after.

Going to try and figure out how to do the "modify in-place" algorithm instead. 
Still unclear how you can modify it in-place without destroying information 
you need whenprocessing cells further along... have to give it a good think. 
I mean, I assume there's a way to do it because this programming website set
it as a challenge.

Licensed under the Mozilla Public License v2.0 like pretty much all of my
code is. Feel free to use it or study it for whatever purpose.